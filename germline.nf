#!/usr/bin/env nextflow

include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

include { procd_fqs_to_alns } from '../alignment/alignment.nf'

include { picard_create_seq_dict } from '../picard2/picard2.nf'


include { deepvariant } from '../deepvariant/deepvariant.nf'
include { samtools_faidx } from '../samtools/samtools.nf'
include { samtools_index } from '../samtools/samtools.nf'

include { bcftools_filter_germline as bcftools_filter_deepvariant} from '../bcftools/bcftools.nf'

workflow make_ancillary_index_files {
// take:
//   dna_ref - Reference Genome FASTA
//   samtools_faidx_parameters - SAMtools faidx Parameters
//
// emit:

// require:
//   params.germline$make_ancillary_index_files$dna_ref
//   params.germline$make_ancillary_index_files$samtools_faidx_parameters
  take:
    ref
    samtools_faidx_parameters
  main:
    samtools_faidx(
      ref,
      samtools_faidx_parameters)
    picard_create_seq_dict(
      ref,
      '')
    //Can't join since the ref symlinks are different.
    samtools_faidx.out.faidx_file
      .concat(picard_create_seq_dict.out.dict_file)
      .collect().map{ [it[0], it[1], it[3]] }
      .set{ collective_idx_files }
  emit:
    collective_idx_files
}

workflow manifest_to_germ_vars {
// require:
//   MANIFEST
//   params.germline$manifest_to_germ_vars$fq_trim_tool
//   params.germline$manifest_to_germ_vars$fq_trim_tool_parameters
//   params.germline$manifest_to_germ_vars$aln_tool
//   params.germline$manifest_to_germ_vars$aln_tool_parameters
//   params.germline$manifest_to_germ_vars$som_var_caller
//   params.germline$manifest_to_germ_vars$som_var_caller_parameters
//   params.germline$manifest_to_germ_vars$som_var_caller_suffix
//   params.germline$manifest_to_germ_vars$aln_ref
//   params.germline$manifest_to_germ_vars$bed
  take:
    manifest
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    germ_var_caller
    germ_var_caller_parameters
    germ_var_caller_suffix
    aln_ref
    bed
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_germ_vars(
      manifest_to_raw_fqs.out.fqs,
      fq_trim_tool,
      fq_trim_tool_parameters,
      aln_tool,
      aln_tool_parameters,
      germ_var_caller,
      germ_var_caller_parameters,
      germ_var_caller_suffix,
      aln_ref,
      bed)
  emit:
    germ_vars = raw_fqs_to_germ_vars.out.germ_vars
    deepvariant_germ_vars = raw_fqs_to_germ_vars.out.deepvariant_germ_vars
}


workflow raw_fqs_to_germ_vars {
// require:
//   FQS
//   params.germline$manifest_to_germ_vars$fq_trim_tool
//   params.germline$manifest_to_germ_vars$fq_trim_tool_parameters
//   params.germline$manifest_to_germ_vars$aln_tool
//   params.germline$manifest_to_germ_vars$aln_tool_parameters
//   params.germline$manifest_to_germ_vars$som_var_caller
//   params.germline$manifest_to_germ_vars$som_var_caller_parameters
//   params.germline$manifest_to_germ_vars$som_var_caller_suffix
//   params.germline$manifest_to_germ_vars$aln_ref
//   params.germline$manifest_to_germ_vars$bed
  take:
    fqs
    fq_trim_tool
    fq_trim_tool_parameters
    aln_tool
    aln_tool_parameters
    germ_var_caller
    germ_var_caller_parameters
    germ_var_caller_suffix
    aln_ref
    bed
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_germ_vars(
      raw_fqs_to_procd_fqs.out.procd_fqs,
      aln_tool,
      aln_tool_parameters,
      germ_var_caller,
      germ_var_caller_parameters,
      germ_var_caller_suffix,
      aln_ref,
      bed)
  emit:
    germ_vars = procd_fqs_to_germ_vars.out.germ_vars
    deepvariant_germ_vars = procd_fqs_to_germ_vars.out.deepvariant_germ_vars
}


workflow procd_fqs_to_germ_vars {
// require:
//   PROCD_FQS
//   params.germline$manifest_to_germ_vars$aln_tool
//   params.germline$manifest_to_germ_vars$aln_tool_parameters
//   params.germline$manifest_to_germ_vars$germ_var_caller
//   params.germline$manifest_to_germ_vars$germ_var_caller_parameters
//   params.germline$manifest_to_germ_vars$germ_var_caller_suffix
//   params.germline$manifest_to_germ_vars$aln_ref
//   params.germline$manifest_to_germ_vars$bed
  take:
    procd_fqs
    aln_tool
    aln_tool_parameters
    germ_var_caller
    germ_var_caller_parameters
    germ_var_caller_suffix
    aln_ref
    bed
  main:
    procd_fqs_to_alns(
      procd_fqs,
      aln_tool,
      aln_tool_parameters,
      aln_ref,
      params.dummy_file)
    alns_to_germ_vars(
      procd_fqs_to_alns.out.alns,
      germ_var_caller,
      germ_var_caller_parameters,
      germ_var_caller_suffix,
      aln_ref,
      bed)
  emit:
    germ_vars = alns_to_germ_vars.out.germ_vars
    deepvariant_germ_vars = alns_to_germ_vars.out.deepvariant_germ_vars
}


workflow alns_to_germ_vars {
// require:
//   ALNS
//   params.germline$manifest_to_germ_vars$germ_var_caller
//   params.germline$manifest_to_germ_vars$germ_var_caller_parameters
//   params.germline$manifest_to_germ_vars$germ_var_caller_suffix
//   params.germline$manifest_to_germ_vars$aln_ref
//   params.germline$manifest_to_germ_vars$bed
  take:
    alns
    germ_var_caller
    germ_var_caller_parameters
    germ_var_caller_suffix
    aln_ref
    bed
  main:
    samtools_index(
      alns,
      '')
    make_ancillary_index_files(
      aln_ref,
      '')

    germ_vars = ''
    deepvariant_germ_vars = ''
    multitools = ''

    if( germ_var_caller =~ /,/ ) {
      println "Running multiple germline variant callers -- use tool-specific output channels."
      multitools = 'True'
    }

    germ_var_caller_parameters = Eval.me(germ_var_caller_parameters)
    germ_var_caller_suffix = Eval.me(germ_var_caller_suffix)

    if( germ_var_caller =~ /deepvariant/ ){
      deepvariant_parameters = germ_var_caller_parameters['deepvariant'] ? germ_var_caller_parameters['deepvariant'] : ''
      deepvariant_suffix = germ_var_caller_suffix['deepvariant'] ? germ_var_caller_suffix['deepvariant'] : ''
      deepvariant(
        samtools_index.out.bams_and_bais,
        make_ancillary_index_files.out.collective_idx_files,
        bed,
        deepvariant_parameters,
        deepvariant_suffix)

      // Setting output channels
      deepvariant.out.germline_vcfs
        .set{deepvariant_germ_vars}
      if( multitools != 'True' ) {
        deepvariant.out.germline_vcfs
          .set{ germ_vars }
      }
    }
  emit:
    deepvariant_germ_vars = deepvariant_germ_vars
    germ_vars = germ_vars
}

workflow germ_vars_to_filtd_germ_vars {
// require:
//   VCFS
//   params.germlin$germ_vars_to_filtd_germ_vars$vcf_filtering_tool
//   params.germlin$germ_vars_to_filtd_germ_vars$vcf_filtering_tool_parameters
  take:
    vcfs
    vcf_filter_tool
    vcf_filter_tool_parameters
  main:
    vcf_filter_tool_parameters = Eval.me(vcf_filter_tool_parameters)
    if( vcf_filter_tool =~ /bcftools/ ) {
      bcftools_filter_deepvariant_parameters = vcf_filter_tool_parameters['deepvariant'] ? vcf_filter_tool_parameters['deepvariant'] : '-i \'FILTER="PASS"\''
      bcftools_filter_deepvariant(
        vcfs.filter{ it[3] =~ /deepv/ },
        bcftools_filter_deepvariant_parameters)
    }
  emit:
    filtd_germ_vars = bcftools_filter_deepvariant.out.filtd_vcfs
}


workflow procd_alns_to_deepvariant_germline_vcfs {
// require:
//   BAMS
//   params.germline$procd_alns_to_deepvariant_germline_vcfs$dna_ref
//   params.germline$procd_alns_to_deepvariant_germline_vcfs$targets_bed
//   params.germline$procd_alns_to_deepvariant_germline_vcfs$deepvariant_model_type
//   params.germline$procd_alns_to_deepvariant_germline_vcfs$deepvariant_parameters
//   params.germline$procd_alns_to_deepvariant_germline_vcfs$deepvariant_suffix
  take:
    bams
    dna_ref
    targets_bed
    deepvariant_model_type
    deepvariant_parameters
    deepvariant_suffix
    samtools_faidx_parameters
  main:
    make_ancillary_index_files(
      dna_ref,
      samtools_faidx_parameters)
    deepvariant(
      bams,
      make_ancillary_index_files.out.collective_idx_files,
      targets_bed,
      deepvariant_model_type,
      deepvariant_parameters,
      deepvariant_suffix)
  emit:
    germline_vcfs = deepvariant.out.germline_vcfs
}
